import smbus2
import time

class MCP23017:
    DEVICE = 0x27 # Device address
    IODIRA = 0x00 # Pin direction register
    IODIRB = 0x01
    OLATA  = 0x14 # Register for outputs
    OLATB = 0x15

    PORTS = {
        'AO' : (OLATA, 0b00000001),
        'A1' : (OLATA, 0b00000010),
        'A2' : (OLATA, 0b00000100),
        'A3' : (OLATA, 0b00001000),
        'A4' : (OLATA, 0b00010000),
        'A5' : (OLATA, 0b00100000),
        'A6' : (OLATA, 0b01000000),
        'A7' : (OLATA, 0b10000000),
        'B0' : (OLATB, 0b00000001),
        'B1' : (OLATB, 0b00000010),
        'B2' : (OLATB, 0b00000100),
        'B3' : (OLATB, 0b00001000),
        'B4' : (OLATB, 0b00010000),
        'B5' : (OLATB, 0b00100000),
        'B6' : (OLATB, 0b01000000),
        'B7' : (OLATB, 0b10000000),
    }

class LightController:

    buses = []

    def __init__(self, bus_numbers=[1, 3, 4]):
        for bus_number in bus_numbers:
            bus = smbus2.SMBus(bus_number)
            bus.write_byte_data(MCP23017.DEVICE, MCP23017.IODIRA, 0)
            bus.write_byte_data(MCP23017.DEVICE, MCP23017.IODIRB, 0)
            self.switch_all_off()
            self.buses.append(bus)

    def switch_all_off(self): 
        for bus in self.buses:
            bus.write_byte_data(MCP23017.DEVICE, MCP23017.OLATA, 0)
            bus.write_byte_data(MCP23017.DEVICE, MCP23017.OLATB, 0)

    def switch_on(self, mcp_number, pin):
        self.buses[mcp_number-1].write_byte_data(MCP23017.DEVICE, *MCP23017.PORTS[pin])

    def switch_off(self, mcp_number, pin):
        if pin.startswith('A'):
            self.buses[mcp_number-1].write_byte_data(MCP23017.DEVICE, MCP23017.OLATA, 0)
        else:
            self.buses[mcp_number-1].write_byte_data(MCP23017.DEVICE, MCP23017.OLATB, 0)


# lights.switch_on(1, 'A7')
# lights.switch_on(2, 'A7')
# lights.switch_on(2, 'B4')
