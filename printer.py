import paho.mqtt.client as mqtt
import json
import os

from escpos import printer

mqtt_host = os.environ['MQTT_HOST']
printer_vendor_id = os.environ['PRINTER_VENDOR_ID']
printer_product_id = os.environ['PRINTER_PRODUCT_ID']

def on_connect(client, userdata, flags, rc):
    client.subscribe("mrc/print")


def on_message(client, userdata, msg):
    if msg.topic == 'mrc/print':
        data = msg.payload.decode('utf-8')
        data = json.loads(data)

        p = printer.Usb(int(printer_vendor_id, 16), int(printer_product_id, 16), profile="TM-L90")
        p.image("mrc.png", center=True)
        # p.qr("http://www.google.com",size=8, center=True)
        p.set(align='center', font='a', width=2, height=2, custom_size=True)
        # # # Print text
        p.text("Liste de vos services\n")
        p.text("numériques\n\n")
        for service in data:
            p.set(align='left', font='a', width=2, height=2, custom_size=True)
            p.text(f"{service['name']}\n")
            p.set(align='left', font='a', width=1, height=1, custom_size=True)
            p.text(f"{service['url']}\n\n")

        p.cut()
        p.close()


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(mqtt_host, 1883, 60)
client.loop_forever()