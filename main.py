import RPi.GPIO as GPIO          
import paho.mqtt.client as mqtt
import json
import os
import time
from decouple import config
from gpiozero import Motor
from gpiozero.pins.pigpio import PiGPIOFactory

from light import LightController

GPIO.cleanup()

GPIO.setmode(GPIO.BCM)

client = mqtt.Client()
mqtt_host = config("MRC_PC_IP", default=None)

if not mqtt_host:
    print("L'adresse IP du PC principal n'a pas été configurée")
    exit(1)

# wait for network to be ready and MRC PC to be up
while os.system(f"ping -c 1 {mqtt_host} > /dev/null 2>&1") != 0:
    print(f"Waiting for {mqtt_host} to be up")
    time.sleep(1)

mrc_client_id = None

pos_cpt = 0
room = None
current_room = None
distance = 0
reset_adjusting = False
room_adjusting = False
reverse = False
previous_reverse = None
room_lights = {}
item_lights = {}

speed = 0.2

motor_switch_pin = 22
GPIO.setup(motor_switch_pin, GPIO.OUT)

pos_switch_pin = 24
GPIO.setup(pos_switch_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

reset_switch_pin = 25
GPIO.setup(reset_switch_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)


def stop_motor():
    global motor
    global pos_cpt
    global room
    global current_room
    global distance
    global room_adjusting
    global reset_adjusting

    time.sleep(.1)
    motor.stop()
    pos_cpt = 0
    distance = 0
    room_adjusting = False
    reset_adjusting = False
    current_room = room
    room = None

    client.publish("mrc/position", json.dumps({
        'room': current_room['slug'],
        'uuid': current_room['uuid'],
    }), qos=1, retain=False)


def reset_switch_pressed(pin):
    global room
    global reset_adjusting
    global motor

    if not room or room["slug"] != "jardin":
        return
    
    print("Reset switch pressed")

    if not reset_adjusting:
        reset_adjusting = True
        time.sleep(.1)
        motor.stop()
        motor.forward(speed)
    else:
        stop_motor()

GPIO.add_event_detect(reset_switch_pin, GPIO.FALLING, callback=reset_switch_pressed, bouncetime=500)


def pos_switch_pressed(pin):
    global motor
    global pos_cpt
    global room
    global current_room
    global distance
    global room_adjusting
    global reverse
    global previous_reverse

    if not room or room["slug"] == "jardin":
        return

    if not room_adjusting:
        dist = distance
        if current_room and current_room["slug"] != "jardin":
            dist += 1 #the current room switch being pressed  due to previous room_adjusting except for at the starting position
        if previous_reverse != reverse:
            dist -= 1
        pos_cpt += 1
        if pos_cpt == dist:
            print(f"STOP at {pos_cpt}")
            time.sleep(.1)
            motor.stop()
            room_adjusting = True
            if reverse:
                motor.forward(speed)
            else:
                motor.backward(speed)
        else:
            print(f"pos switch pressed {pos_cpt}")
    else:
        stop_motor()

GPIO.add_event_detect(pos_switch_pin, GPIO.FALLING, callback=pos_switch_pressed, bouncetime=500)


motor = Motor(6, 16, 5, pwm=True, pin_factory=PiGPIOFactory())
motor.stop()
GPIO.output(motor_switch_pin, GPIO.HIGH)

try:
    item_light_ctrl = LightController()
except:
    item_light_ctrl = None

def on_connect(client, userdata, flags, rc):
    client.subscribe("mrc/item")
    client.subscribe("mrc/debug")
    client.subscribe("mrc/mode")
    client.subscribe("mrc/rotate")
    client.subscribe("mrc/reset")

def on_message(client, userdata, msg):
    global distance
    global pos_cpt
    global room
    global current_room
    global room_adjusting
    global room_lights
    global item_lights
    global mrc_client_id
    global reverse
    global previous_reverse

    print(f"Received {msg.topic} with payload {msg.payload.decode('utf-8')}")
    
    if msg.topic == 'mrc/rotate':
        data = msg.payload.decode('utf-8')
        room = json.loads(data)
        distance = abs(room['distance'])

        previous_reverse = reverse if previous_reverse else room['reverse']
        reverse = room['reverse']

        if distance == 0:
            #do nothing for the moment
            pos_cpt = 0
            distance = 0
            room_adjusting = False
            current_room = room
            room = None

            client.publish("mrc/position", json.dumps({
                'room': current_room['slug'],
                'uuid': current_room['uuid'],
            }), qos=1, retain=False)
            return

        if reverse or room['slug'] == 'jardin':
            motor.backward(speed)
        else:
            motor.forward(speed)
    elif msg.topic == 'mrc/reset':
        pos_cpt = 0
        room = None
        current_room = None
        distance = 0
        room_adjusting = False
        reverse = False

    elif msg.topic == 'mrc/item':
        data = msg.payload.decode('utf-8')
        item = json.loads(data)
        
        if not item:
            # turn off all item lights
            for idx in item_lights:
                item_light_ctrl_nb, item_light_pin = idx.split('-')
                if item_light_ctrl:
                    item_light_ctrl.switch_off(int(item_light_ctrl_nb), item_light_pin)
                item_lights[idx] = False
        elif 'lightCtrl' in item and 'lightPin' in item \
            and item['lightCtrl'] is not None and item['lightPin'] is not None:

            item_light_ctrl_nb = int(item['lightCtrl'])
            item_light_pin = item['lightPin']
            idx = f"{item_light_ctrl_nb}-{item_light_pin}"
            if item_light_ctrl:
                    item_light_ctrl.switch_on(item_light_ctrl_nb, item_light_pin)
            item_lights[idx] = True
    
    elif msg.topic == 'mrc/mode':
        data = msg.payload.decode('utf-8')
        data = json.loads(data)
        print("mrc/mode Data: {}".format(data))
        mode = data["mode"]
        unique_id = data["uniqueId"]
        type = data["type"]
        if type == "REQ":
            if (mrc_client_id is None or mrc_client_id == unique_id) or mode == 'primary':
                mrc_client_id = unique_id
                client.publish("mrc/mode", json.dumps({
                    "mode": mode,
                    "uniqueId": unique_id,
                    "type": "RESP",
                    "value": "OK"
                }))
            else:
                client.publish("mrc/mode", json.dumps({
                    "mode": mode,
                    "uniqueId": unique_id,
                    "type": "RESP",
                    "value": "KO"
                }))

        elif type == "RST" and unique_id == mrc_client_id:
            mrc_client_id = None
        
    elif msg.topic == 'mrc/debug':
        data = msg.payload.decode('utf-8')
        data = json.loads(data)
        if 'action' in data:
            action = data['action']
            if action == 'motor':
                if motor.is_active:
                    motor.stop()
                value = data['value'].lower()
                GPIO.output(motor_switch_pin, GPIO.HIGH if value == 'off' else GPIO.LOW)
            elif action == 'room_light':
                room_light_pin = int(data['pin'])
                if room_light_pin in room_lights:
                    value = GPIO.HIGH if room_lights[room_light_pin] == GPIO.LOW else GPIO.LOW
                    GPIO.output(room_light_pin, value)
                    room_lights[room_light_pin] = value
                
                else:
                    GPIO.setup(room_light_pin, GPIO.OUT)
                    GPIO.output(room_light_pin, GPIO.LOW)
                    room_lights[room_light_pin] = GPIO.LOW
            elif action == 'item_light':
                item_light_ctrl_nb = int(data['lightCtrl'])
                item_light_pin = data['lightPin']
                idx = f"{item_light_ctrl_nb}-{item_light_pin}"
                if idx in item_lights:
                    value = not item_lights[idx]
                else:
                    value = True

                if item_light_ctrl:
                    if value:
                        item_light_ctrl.switch_on(item_light_ctrl_nb, item_light_pin)
                    else:
                        item_light_ctrl.switch_off(item_light_ctrl_nb, item_light_pin)
                
                item_lights[idx] = value
            elif action == 'reset_client':
                mrc_client_id = None


client.on_connect = on_connect
client.on_message = on_message

client.connect(mqtt_host, 1883, 60)
client.loop_forever()
